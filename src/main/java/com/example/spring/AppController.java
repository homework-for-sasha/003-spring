package com.example.spring;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.awt.*;
import java.util.Map;

@Controller
public class AppController {

    @GetMapping("/")
    public String getHome() {
        return "home";
    }

    @GetMapping("/home")
    public String getSomeText(String q, Integer h) {
        System.out.println(h);
        if (q != null) {
            return "home";
        } else {
            return "test";
        }
    }

    @PostMapping(value = "/", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},
    produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public String postHome(@RequestParam Map<String, String> user) {
        if (user != null) {
            System.out.println(user.get("name"));
            System.out.println(user.get("age"));
        }
        return "home";
    }
}




